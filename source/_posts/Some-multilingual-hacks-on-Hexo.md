---
alttitle: Some multilingual hacks on Hexo
title: >-
  <span class='ts-langs-group'>
  <span class='ts-lang-en ts-langs ts-langs-show ts-langs-block'>
  Some multilingual hacks on Hexo
  </span>
  <span class='ts-lang-zh-CN ts-langs ts-langs-show ts-langs-block'>
  瞎折腾Hexo的多语言
  </span>
  </span>
date: 2020-10-10 16:55:15
tags:
  - hexo
  - javascript
  - css
  - 坑
---

{% langwrap %}

{% lang en %}
I do not know why exactly I am so stubborn to insist on making this
site multilingual, and in a way that you may see multiple languages
at the same time. But it is something.
{% endlang %}
{% lang zh-CN hide %}
我自己也不明白为什么我执拗得一定要把这网站做成多语言的，还是叫人
能同时看到多种语言的那种。但聊胜于无吧。
{% endlang %}

<!-- more -->

# {% langgrp %}{% lang en inline %}A weird thing about Hexo{% endlang %}{% lang zh-CN hide inline %}Hexo 的诡异事件{% endlang %}{% endlanggrp %}

{% lang en %}
The *first* weird thing I got, when I was trying to figure out
[hexo-theme-next][next-theme]: sometimes `hexo server` gives
the perfect result, but when I `hexo generate` or `hexo deploy`,
nothing will be correct. Everything is just malformed, and it
does not change even if I `hexo clean`.
{% endlang %}
{% lang zh-CN hide %}
我在折腾 [hexo-theme-next][next-theme] 的时候碰到的第一个诡异事件是：
有时候 `hexo server` 给出了个无懈可击的结果，但只要一 `hexo generate`
或者 `hexo deploy`，全完蛋。整个网站看着都非常诡异，甚至我
`hexo clean` 了之后也没有丝毫变化。
{% endlang %}

[next-theme]: https://github.com/next-theme/hexo-theme-next

{% lang en %}
After digging a lot into the source code, and enabling `--debug`,
I finally found that there are some backup files that are also
processed. And the content of those files *does* correspond to
the wrong contents on the site. "Okay," hereby I say, "this
behaviour is indeed ridiculous."
{% endlang %}
{% lang zh-CN hide %}
钻进源码里东张西望了好久，再合着 `--debug` 的加成，我终于看到了一些备份文件
也被处理了。而其内容**确然**是应了网站上有问题的东西。「行吧，」如是我说，
「是事直令人齿冷。」
{% endlang %}

{% lang en %}
And thus I added the ignore rules to ignore all such files,
and everything is hopefully back in place.
{% endlang %}
{% lang zh-CN hide %}
所以我加了忽略规则来忽略这种文件，世界终于清净了，谢天谢地。
{% endlang %}

```yaml
ignore:
  - "**/*~"
  - "**/#*"
  - "**/.#*"
```

# {% langgrp %}{% lang en inline %}NexT's language switcher{% endlang %}{% lang zh-CN hide inline %}NexT 的语言切换器{% endlang %}{% endlanggrp %}

{% lang en %}
That is an even more terrible part that comes in. Yes,
[NexT][next-theme] comes with such a thing called a
"language switcher," but it never takes you to a right
place. I cannot even get Hexo generate pages for
different languages, no matter how I change the `language`
parameter in `_config.yml`. So whenever I click on the
language switcher, I got a 404. And it is hidden at the very
bottom of the page --- How should one expect their visitor
to dig into the deepest hole before they can switch languages?
{% endlang %}
{% lang zh-CN hide %}
这玩意儿甚至更可怕了。是是是，[NexT][next-theme] 有个叫「语言切换器」
的东西，但是它总把人引入歧途。让 Hexo 给不同的语言分别生成页面，这我
怎么改 `_config.yml` 里的 `language` 参数，也弄不成。所以呢，每次
按下这语言切换器的时候，404 就浮现在我眼前了。而且这切换器给藏在了页面
最底下——让访客钻进最深的洞里才许其切换语言，这能成吗？
{% endlang %}

{% lang en %}
So, well, since NexT's language switcher sucks, and multi-page
style multi-language implementation is not feasible, why not just
put everything all together? NexT at least provides a way to
customize what texts are shown... in `languages.yml`.
{% endlang %}
{% lang zh-CN hide %}
啊啊，行吧，NexT 这语言切换器是糟透了，多页面显示多语言也办不到，那么把
所有东西全放一起，这又如何呢？NexT 至少给了个用 `languages.yml` 来控制
显示什么文字的方法呀......
{% endlang %}

{% lang en %}
The main idea is to merge NexT's default translations into one.
I made a Perl script for this. The text for every language is a
`span`, and they are next to each other. Use css to put a divider
between every visible `span`. Whether a language is shown is controlled
by a JavaScript program that adds or removes classes from the elements.
{% endlang %}
{% lang zh-CN hide %}
大意就是把 NexT 的默认翻译全整一起去。这事我写了个 Perl 脚本去做。每种语言的文字
装在一个 `span` 里，并并排。用 css 在每个显示了的 `span` 之间，摆一个分割线。
至于某种语言是显示呢是隐藏呢，这让 JavaScript 程序来控制，就从元素里加上或是减去
些 class 就好。
{% endlang %}

{% lang en %}
As I am too lazy, the code is all [on GitLab][gitlab-repo].
Whether this is a viable implementation is left as an exercise
for the reader.
{% endlang %}
{% lang zh-CN hide %}
因为我太懒了，代码都在 [GitLab 上][gitlab-repo]。这东西到底可行不，
留给读者作练习。
{% endlang %}

[gitlab-repo]: https://gitlab.com/tusooa/blog

{% endlangwrap %}
