---
title: "Fix firefox address bar clickSelectsAll bug (Spoiler: no re-compilation required.)"
date: 2020-10-01 20:15:56
tags:
  - firefox
  - 无奈
  - 坑
  - javascript
---

A couple of months ago, I opened my Firefox as usual, I found out that
when I click on the address bar, it dared to select everything in it
as if any creepy browser would do! The first thing I did is to check
the `clickSelectsAll` pref -- what I always do first after getting a
new install of Firefox on Windows is setting this pref to `false`,
and setting `doubleClickSelectsAll` pref to `true`: that is the default
behaviour for Firefox on GNU/Linux... that time at least...

Much to my disappointment, the pref are setting correctly, but nothing
works. I tried to search for this, but without success, maybe since
this bug was so new, or I was to silly to apply the right keywords
(huh, maybe the latter one?). So I created an account on Mozilla's
bugtracker just to open [a bug][my-bug]. At the same time I switched
back to the ESR version of Firefox, which rid me (till recently) of
this stupid address bar but still granted me security updates. A
few days ago, however, Firefox ESR was unfortunately eventually shipped
with this bug, calling for a solution.

[my-bug]: https://bugzilla.mozilla.org/show_bug.cgi?id=1629135

<!-- more -->

# Some more background

Here I will add more background because I need to complain a lot (>w<).
If you would like to see the solution directly, [jump there](#The-solution).

Someone on bugzilla pointed out that the bug had been
[raised by someone else][orig-bug]. Following that bug, I saw that
the developer was being pretty toxic and just marked it as `WONTFIX`,
despite many people criticized the buggy behaviour and regarded
the original behaviour as signature of Firefox. I finally switched
from Ubuntu to Gentoo after the new ESR, as I would like to have more
control of my software.

[orig-bug]: https://bugzilla.mozilla.org/show_bug.cgi?id=1621570

Yesterday I [talked][freedom-talk] to [tiar][tiar] about the freedom
of users to make software behave the way they want, without necessarily
being a developer. I complained that many free/libre software does not
excel in this aspect, using Firefox \>=75 as an example.

[tiar]: https://outsideofinfinity.wordpress.com/

[freedom-talk]: https://matrix.to/#/!NREUQXSZzhWoQFQpTe:matrix.org/$16014845914171808dKKcm:matrix.org?via=matrix.org&via=tusooa.xyz

The thing Firefox developers does, by removing the `clickSelectsAll`
prefs, is actually somewhat limiting users' freedom, as customization
becomes harder and harder. This kind of freedom has been brought up
by a KDE person as [a goal][out-of-box] called "freedom out of the box."
In short, it aims to make changing the behaviour of a program
as easy as just using it, probably unifying the interface where a program
is used and where a program is customized.

[out-of-box]: https://phabricator.kde.org/T11091

And that is reasonable: no one should have to spend quite a long time
reading an extraordinarily large code base just to make a tiny little
change that would significantly improve their lives. We will simply not
have that much time. I know tweaking system is fun... (should I say
addictive?) but still, instant result is always a good-to-have.

# The solution

For Firefox, we do have a [solution][solution] without needing to
recompile. That is, to change the `omni.ja` pack in the Firefox
distribution. Based on that I managed to restore the old good
urlbar behaviour.

[solution]: https://superuser.com/questions/540851/go-back-to-not-selecting-the-whole-url-when-i-click-the-address-bar

For my `firefox-bin` on Gentoo, the relevant file is at
`/opt/firefox/browser/omni.ja`.

First make a copy of that file:

```bash
cp -v /opt/firefox/browser/omni.ja omni.ja.orig
```

Then unzip the file:

```bash
mkdir omni && unzip -d omni /opt/firefox/browser/omni.ja
```

It may return 2, but does not matter.

Then edit the `modules/UrlbarInput.jsm`:

1. Change
   ```javascript
   this._preventClickSelectsAll = this.focused;
   ```
   to
   ```javascript
   this._preventClickSelectsAll = true;
   ```
   to get the `clickSelectsAll` behaviour.

2. Change
   ```javascript
   if (event.target.id == SEARCH_BUTTON_ID) {
   ```
   to
   ```javascript
         if (event.detail == 2) {
           this.select();
           event.preventDefault();
         } else if (event.target.id == SEARCH_BUTTON_ID) {
   ```
   to get the `doubleClickSelectsAll` behaviour.

   Note that this will *not* put the url into primary selection
   upon double click. This can be useful sometimes, if you just
   would like to replace the current url with the one currently
   in primary selection.
   If you would like to put the url into primary selection
   upon double click, which completely restores the original
   behaviour, change it to
   ```javascript
         if (event.detail == 2) {
           this.inputField.select();
           event.preventDefault();
         } else if (event.target.id == SEARCH_BUTTON_ID) {
   ```
   instead.

I have also made a patch:
```bash
] diff -Naur omni-old omni
diff -Naur omni-old/modules/UrlbarInput.jsm omni/modules/UrlbarInput.jsm
--- omni-old/modules/UrlbarInput.jsm    2010-01-01 00:00:00.000000000 -0500
+++ omni/modules/UrlbarInput.jsm        2020-10-01 19:50:41.925266392 -0400
@@ -2225,7 +2225,7 @@
         }

         this.focusedViaMousedown = !this.focused;
-        this._preventClickSelectsAll = this.focused;
+        this._preventClickSelectsAll = true;

         if (event.target != this.inputField) {
           this.focus();
@@ -2242,7 +2242,11 @@
           this.selectionStart = this.selectionEnd = 0;
         }

-        if (event.target.id == SEARCH_BUTTON_ID) {
+        // doubleClickSelectsAll
+        if (event.detail == 2) {
+          this.select();
+          event.preventDefault();
+        } else if (event.target.id == SEARCH_BUTTON_ID) {
           this._preventClickSelectsAll = true;
           this.search(UrlbarTokenizer.RESTRICT.SEARCH);
         } else {
```

There is also the search bar to change. The original Superuser answer
suggests this, but as I do not use the search bar, I have not
tried it out, nor do I know what the original behaviour of it was.
```bash
sed -i 's/this\._preventClickSelectsAll = this\._textbox\.focused;/this._preventClickSelectsAll = true;/'  omni/chrome/browser/content/browser/search/searchbar.js
```

After editing, we need to re-pack the files:
```bash
cd omni && zip -0DXqr ../omni.ja * && cd -
```

And replace the original one:
```bash
cp -v omni.ja /opt/firefox/browser/omni.ja
```

You may need to change the ownership of the newly-created archive:
```bash
chown [U]:[G] /opt/firefox/browser/omni.ja
```
where `[U]` and `[G]` refers to the owner and group of the original
file (we made a backup at `omni.ja.orig`).

To ensure everything takes effect, make a file called `.purgecaches`
to make Firefox aware of the changes:
```bash
touch /opt/firefox/browser/.purgecaches
```

Okay, from now on, we shall no longer suffer from the `clickSelectsAll`
bug.

Also, you may want to have additional configurations, such as
[disabling the pop-up list upon clicking address bar][disable-popup]
or [gettind rid of the address bar expanding effect][disable-expanding]
when clicked upon.

[disable-popup]: https://www.userchrome.org/megabar-styling-firefox-address-bar.html
[disable-expanding]: https://github.com/WesleyBranton/Remove-Firefox-Megabar/blob/master/remove_megabar.css
