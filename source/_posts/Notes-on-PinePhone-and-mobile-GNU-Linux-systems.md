---
alttitle: Notes on PinePhone and mobile GNU/Linux systems
title: >-
  <span class='ts-langs-group'>
  <span class='ts-lang-en ts-langs ts-langs-show ts-langs-block'>
  Notes on PinePhone and mobile GNU/Linux systems
  </span>
  <span class='ts-lang-zh-CN ts-langs ts-langs-show ts-langs-block'>
  关于 PinePhone 和移动端 GNU/Linux 系统的笔记
  </span>
  </span>

date: 2021-03-28 19:07:52
tags:
  - GNU/Linux
  - 坑
  - PinePhone
---

{% langwrap %}

{% lang en %}
At the end of last year, I bought the PinePhone KDE Community Edition; but due to me being too lazy,
I had not tried it out until several months later. I used a few distributions, namely (1) the manjaro
with Plasma Mobile it comes with, (2) Plasma Mobile development version, also based on Manjaro, and
(3) openSUSE Tumbleweed with Plasma Mobile. The first two are not really usable, as I got blackscreens
after a few reboots -((.
{% endlang %}
{% lang zh-CN hide %}
去年底，我买了 PinePhone KDE 社区版；然而因为我太懒，好几个月都没得好好试试。用了几个发行版，分别是 (1)
PinePhone 自带的 Manjaro，(2) Plasma Mobile 的开发版本，也是基于 Manjaro 的，和 (3) 带了 Plasma Mobile
的 openSUSE Tumbleweed。前两个都用不起来，重启几次之后就黑屏了 -((。
{% endlang %}

<!-- more -->

# {% langgrp %}{% lang en inline %}Experience with openSUSE{% endlang %}{% lang zh-CN hide inline %}openSUSE 的体验{% endlang %}{% endlanggrp %}

## {% langgrp %}{% lang en inline %}What's working{% endlang %}{% lang zh-CN hide inline %}什么能用{% endlang %}{% endlanggrp %}

1.
  {% lang en %}
  Messaging and calls. Had a call with my girlfriend for about 30mins without interruption. Although she said my voice is a bit strange.
  {% endlang %}
  {% lang zh-CN hide %}
  短信和通话。给女朋友打了三十分的电话，没得中断过的。虽然她说我的声音有一点奇怪。
  {% endlang %}

2.
  {% lang en %}
  NeoChat.
  {% endlang %}
  {% lang zh-CN hide %}
  NeoChat。
  {% endlang %}

3.
  {% lang en %}
  Web browser.
  {% endlang %}
  {% lang zh-CN hide %}
  网络浏览器。
  {% endlang %}

4.
  {% lang en %}
  Setting locales. I see some translations in Mandarin as Simplified Chinese are there already, but
  there are a lot of places without translations (namely those specific to Plasma Mobile).
  {% endlang %}
  {% lang zh-CN hide %}
  设置 locale。我看到有的简体中文的官话翻译已经在那块了，但是还是有好多地方没得个翻译的（就那些只在
  Plasma Mobile 里头有的）。
  {% endlang %}


## {% langgrp %}{% lang en inline %}Problems{% endlang %}{% lang zh-CN hide inline %}问题{% endlang %}{% endlanggrp %}

1.
  {% lang en %}
  Data does not work. Plasma Mobile does not give me an option to [change network settings][apn] either.
  {% endlang %}
  {% lang zh-CN hide %}
  流量没得办法用。Plasma Mobile 也没得给我个[改网络设置][apn]的地方哎。
  {% endlang %}

[apn]: https://wiki.pine64.org/wiki/PinePhone_APN_Settings#Setting_the_APN

2.
  {% lang en %}
  Sometimes laggy.
  {% endlang %}
  {% lang zh-CN hide %}
  有的时候卡。
  {% endlang %}

3.
  {% lang en %}
  Due to 2, pushing the power button does not always immediately wake up the phone.
  {% endlang %}
  {% lang zh-CN hide %}
  因为 2，按电源键了，也不一定能立刻唤醒手机。
  {% endlang %}

4.
  {% lang en %}
  Input method does not work. At least for the openSUSE distribution, it (maliit) does not come with Chinese input.
  Even after I installed `maliit-keyboard2-lang`. The screen keyboard is implemented using IM modules.
  Setting `QT_IM_MODULE=fcitx` will just prevent the screen keyboard from showing up.
  {% endlang %}
  {% lang zh-CN hide %}
  输入法没得办法用。至少在 openSUSE 发行版高头，丫 (maliit) 没得个中文输入的。就算我装了 `maliit-keyboard2-lang`，
  也不行。屏幕键盘用输入法模块实现的。设个 `QT_IM_MODULE=fcitx`，屏幕键盘就不能显示了。
  {% endlang %}

5.
  {% lang en %}
  With earphones there are electrical current sounds from time to time.
  {% endlang %}
  {% lang zh-CN hide %}
  耳机有的时候会有电流声。
  {% endlang %}

6.
  {% lang en %}
  If I click an app, and switch to another app, or the desktop, before that app window
  shows up, the app window will pop up afterwards instead of being pushed to the background.
  {% endlang %}
  {% lang zh-CN hide %}
  要是我按一个应用，再在那个应用的窗口显示之前，切到另一个应用，或者是桌面，那那个窗口会弹出来，
  而不是摁到后台去。
  {% endlang %}

7.
  {% lang en %}
  Privacytools search reports "Rate limit exceeded1" [sic] when I search in Angelfish.
  {% endlang %}
  {% lang zh-CN hide %}
  我在 Angelfish 里搜索的时候，Privacytools 的搜索跟我讲「Rate limit exceeded1」[原文如此]。
  {% endlang %}

8.
  {% lang en %}
  The screen keyboard sometimes responds to key pressing with word suggestions, which can
  be never inputed to the input box desired.
  {% endlang %}
  {% lang zh-CN hide %}
  屏幕键盘有的时候会把我的按键转成单词建议，但那玩意儿又从来没得办法输到输入框里去。
  {% endlang %}

# {% langgrp %}{% lang en inline %}Other notes{% endlang %}{% lang zh-CN hide inline %}别的笔记{% endlang %}{% endlanggrp %}

1.
  {% lang en %}
  I have a nano sim card and it will need the adapter that comes with PinePhone to install it properly. I
  pushed it in without the adapter, and had to disassemble the phone to push it back out from the inside.
  {% endlang %}
  {% lang zh-CN hide %}
  我有个 nano sim card，那玩意儿要用 PinePhone 自带的转接器才能好好装上。之前没得用转接器就摁进去了，结果
  只能把手机拆的了然后从里头给它推出来。
  {% endlang %}

2.
  {% lang en %}
  The Manjaro distribution the PinePhone comes with has Telegram installed by default. Telegram can only
  connect to a non-free server, and forces users to provide a phone number in order to use services that
  does not need one. This is a serious privacy concern.
  I strongly oppose to the use of Telegram in any cases, and suggest everyone uninstall
  it upon receiving your PinePhone.
  {% endlang %}
  {% lang zh-CN hide %}
  PinePhone 自带的 Manjaro 发行版默认安装了 Telegram。Telegram 只能连接到一个不自由的服务器，而且强迫用户
  提供手机号以使用并不需要手机号的服务。这是一个严重的隐私隐患。
  我强烈反对任何情况下使用 Telegram，并推荐所有人收到 PinePhone 之后立刻卸载它。
  {% endlang %}

3.
  {% lang en %}
  The Manjaro distribution the PinePhone comes with cannot be waken up by an alarm clock. I have not yet
  tried with openSUSE.
  {% endlang %}
  {% lang zh-CN hide %}
  PinePhone 自带的 Manjaro 发行版没得办法被闹钟唤醒。openSUSE 我还没得试过。
  {% endlang %}



{% endlangwrap %}
