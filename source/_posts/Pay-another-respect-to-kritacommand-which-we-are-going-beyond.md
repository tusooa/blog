---
title: Pay another respect to kritacommand--which we are going beyond
date: 2019-08-24 19:51:09
tags:
 - kde
 - krita
 - tech
 - c++
---

> Your work is gonna make Krita significantly different.
> -- [Wolthera](https://invent.kde.org/woltherav), Krita developer and digital artist

Krita's undo system, namely `kritacommand`, was [added](https://invent.kde.org/kde/krita/commit/76fc48a7ce15afb93c072d4faa77da831517ee52) 8 years ago to Calligra under the name of `kundo2`, as a fork of Qt's undo framework. The use of undo commands, however, might have an even longer history. Undo commands provide a way to revert individual actions. Up to now, most ([though not all](https://invent.kde.org/kde/krita/merge_requests/32/diffs)) undo commands do it by providing two sets of code that do and undo the actions, respectively. Drawbacks of this system includes (1) it is not very easy to manage; (2) it may introduce duplicated code; and (3) it makes it hard to access a previous document state without actually going back to that state. What I do is to start getting rid of such situation.

The plan for a new system is to use shallow copies to store documents at different states. Dmitry said "it was something we really want to do and allows us to make historical brushes (fetch content from earlier document states)." And according to him, he spent years to implement copy-on-write on paint layers. He suggested me to start from vector layers which he thought would be easier since it does not need to be very thread-safe. 

I completely understood that was a challenge, but did not realize where the difficult part was until I come here. Copy-on-write is not the challenging part. We have `QSharedDataPointer` and almost all the work is to routinely replace the same code. Porting tools is more difficult. The old flake tools are running under the GUI thread, which makes no requirement on thread-safety. Technically we do not need to run it in a stroke / in image thread but with no multithreading the tools runs too slowly on some computers (read as "my Thinkpad laptop") so I am not unwilling to take this extra challenge. In previous posts I described how the strokes work and the problems I encountered. Besides that there are still some problems I need to face.

## the HACK code in the stroke strategy

At the last of the strokes post, I proposed a fix to the crash when deleting `KisNode`, which is messy. After testing with Dmitry at the sprint, we discovered that the real problems lies in `KoShapeManager`'s `updateTreeCompressor`. It is used to schedule updates of its R-tree. However, it is run at the beginning of every other operation so Dmitry says it is no longer needed. After the compressor was removed we are safe to delete the node normally so there would be no need for such hack code.

## Path tool crashing when editing calligraphic shapes

Calligraphic shapes, coming from Karbon, is a shape created by hand-drawing. It has many path points and editing it using path tool usually leads to a crash. Dmitry tested it with ASan and discovered the problem occurs because the path points, which is fetched in the GUI thread to paint the canvas, could be deleted when editing the shape. He suggests to apply a lock to the canvas, not allowing the image and GUI threads to access the shapes concurrently.

## Keeping selections after undo/redoing

This challenge is a smaller one. The shape selections were not kept, since they are not part of the layer. It was owned by the layer's shape manager, though, but a cloned layer would take a brand-new shape manager. In addition `undo()` and `redo()` will now replace the whole layer, so pointers to original shapes are no longer valid. This means merely keeping the selections from the shape manager would not work. The solution is to map the selected shapes to the cloned layer, which would be kept in the undo command. The strategy I use is similar to what we have done for layers: go through the whole heirarchy of the old layer and push everything into a queue; go through the heirarchy of the cloned layer in the same order and each time take the first shape in the queue; if the popped shape is in the selection, we add its counterpart in the cloned layer to our new selection.

For now the tools should be working and the merge request is prepared for final review. Hopefully it would make its way to `master` soon.
