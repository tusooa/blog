---
title: 'Assistants -- copy, share, assignment'
date: 2019-05-30 14:20:53
tags:
 - kde
 - krita
 - c++
 - code
---

Over the last week I have been investigating into [Bug 361012](https://bugs.kde.org/show_bug.cgi?id=361012), 
on the undo history of the modification of guides. But from the very beginning I mixed up the two terms 
"guides" and "assistants," so I decided to [work on both](https://invent.kde.org/kde/krita/merge_requests/32). 
The work with guides is a lot simpler and will not be covered here, though. 

As I write this post, the [master branch of Krita](https://invent.kde.org/kde/krita/tree/master) 
does not create any undo commands for the document. I first [added undo commands](https://invent.kde.org/kde/krita/merge_requests/32/diffs?commit_id=12bc1d0730f04321c5ec304ce09f6670ef14c576) for adding and removing 
assistants, which seems the easiest. The editing of them is a bit more difficult, as the dragging 
operations involve the movement of many "handles," the movable round buttons that define the position 
of one or more assistants. The source code on master for implementing such actions is quite complicated 
and involves a great number of cases. It would be another great endeavour to put all these bunches of 
code into a `KUndo2Command`. But, another thing I have experimented with and I will be working on 
will immediately clear the clouds.

So I just thought of the copy-on-write mechanism, and yes, why not? Though COW itself is not 
actually implemented for the guides, it does seem inspiring. I mean, we can just save a copy 
of all assistants and, when needed, restore that. 

The main problem here is the handles. They are represented as shared pointers in individual 
assistants and may be shared between different ones (e.g. two perspectives share two corner 
handles and one side handles). When we take a clone of the list of assistants it will be 
necessary to keep this kind of relationship. My solution is to use a `QMap` of pointers, 
which seems to coincide with the logic of exporting to xml, but I had yet to read that part
of the code when writing mine so I did not know about that. The logic is to check, for 
every handle, whether there is a mapping relationship in the map. If there is, we reuse that 
handle, and if not, we create a new one with the same position and record that relationship 
in our `QMap`.

But some display properties are not to be recorded into the undo history. Such properties 
include the changing of color, visibility, etc. To resolve this problem, I put these data into 
a shared pointer and, when we are cloning an assistant for undo/redo, we will reuse that pointer.
When we replace the assistant list with the one recorded, all the display properties will remain 
since the data are shared. 

And for the next several weeks I will move onto the [Snapshot Docker](https://phabricator.kde.org/T10991).
