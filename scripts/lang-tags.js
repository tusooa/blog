/* global hexo */

/*
* {% lang [lang-code] [show='show'|'hide' (def. show)] [is-block='block'|'inline' (def. inline)] %}
*  content
* {% endlang %}
*/

hexo.extend.tag.register('lang', function (args, content) {
  const lang = args[0];
  let show = true;
  let isBlock = true;
  for (const opt of args.slice(1)) {
    if (opt === 'hide') {
      show = false;
    } else if (opt === 'inline') {
      isBlock = false;
    }
  }

  let tagName = isBlock ? 'div' : 'span';
  let result = `<${tagName} class='ts-lang-${lang} ts-langs${show ? ' ts-langs-show' : ''}${isBlock ? ' ts-langs-block' : ''}'>`;
  result += content;
  result += `</${tagName}>`;
  return result;
}, true);

/*
* {% langgrp %}
*  content
* {% endlanggrp %}
*/

hexo.extend.tag.register('langgrp', function (args, content) {
  let result = "<span class='ts-langs-group'>";
  result += content; //hexo.render.renderSync({text: content, engine: 'markdown'});
  result += '</span>';

  return result;
}, true);

hexo.extend.tag.register('langwrap', function (args, content) {
  return '<style type="text/css">.ts-langs:not(.ts-langs-show) { display: none; }</style>' +
    hexo.render.renderSync({text: content, engine: 'markdown'});
}, true);
