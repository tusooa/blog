#!/usr/bin/env perl

use YAML::XS;
use JSON::XS;
use Encode qw/_utf8_off/;
use 5.018;

my @langs = ('en', 'zh-CN');

my %srcFiles = map {; $_ => "node_modules/hexo-theme-next/languages/$_.yml" } @langs;
my $dstFile = 'source/_data/languages.yml';

my @ignored = ('name', 'symbol');
my @blocks = ('cheers', 'counter', 'keep_on');

my $output = {};

### CONFIG START
my $langClassPrefix = 'ts-lang-';
my $langsClass = 'ts-langs';
my $shownLangsClass = 'ts-langs-show';
my $blockLangsClass = 'ts-langs-block';
my $langsGroupClass = 'ts-langs-group';
### CONFIG END

sub langClassFor {
    my $lang = shift;
    "$langClassPrefix$lang"
}

sub classFor
{
    my ($lang, $isBlock) = @_;
    langClassFor($lang)." $langsClass $shownLangsClass".($isBlock ? " $blockLangsClass" : '');
}

sub pathJoin
{
    my ($parent, $child) = @_;
    $parent ? "$parent/$child" : $child;
}

sub walkStruct
{
    my ($lang, $struct, $output, $attr) = @_;
    $attr or $attr = {};

    for my $k (keys %$struct) {
        if ($k ~~ @ignored) {
            next;
        }
        my $thisAttr = { %$attr };
        $thisAttr->{path} = pathJoin $thisAttr->{path}, $k;
        say "PATH: $thisAttr->{path}";
        if ($thisAttr->{path} ~~ @blocks) {
            $thisAttr->{block} = 1;
        }
        if (ref $struct->{$k} eq 'HASH') {
            if (not $output->{$k}) {
                $output->{$k} = {};
            }
            my $subAttr = { %$thisAttr };
            walkStruct($lang, $struct->{$k}, $output->{$k}, $thisAttr);
        } else { # it's a string
            if (not defined $output->{$k}) {
                $output->{$k} = '';
            }
            my $text = $struct->{$k};
            # Handle multiple %s etc.
            my $n = 1;
            $text =~ s/%(.)/'%'.($n++).'$'.$1/e;

            my $lastLang;
            $lastLang = $1 while $output->{$k} =~ /\Q$langClassPrefix\E([A-Za-z-]+)/g;

            $output->{$k} .= #($lastLang ? "<span class='ts-lang-div-${lastLang} ts-lang-div'></span>" : '')
                "<span class='".(classFor $lang, $thisAttr->{block})
                ."'>".$text.'</span>';
                #$struct->{$k};
        }
    }
}

sub dumpStruct
{
    my ($struct, $indent) = @_;
    my $str = '';
    for my $k (sort keys %$struct) {
        if (ref $struct->{$k} eq 'HASH') {
            $str .=(' ' x $indent)."$k: \n"
                .dumpStruct($struct->{$k}, $indent + 2)."\n";
        } else { # it's a string
            $str .= (' ' x $indent)."$k: "
                #.">-\n"
                #.(' ' x ($indent + 2))
                .'"'
                ."<span class='${langsGroupClass}'>"
                .$struct->{$k}
                ."</span>"
                .'"'
                ."\n";
        }
    }
    $str;
}

for my $lang (@langs) {
    my $f = $srcFiles{$lang};
    open FILE, '<', $f or die "Cannot open $f: $!\n";
    local ($/);
    my $data = <FILE>;
    my $struct = Load $data;
    walkStruct $lang, $struct, $output;
    close FILE;
}

open OUT, '>', $dstFile or die "Cannot open $dstFile: $!\n";
binmode OUT, ':unix';

my $text = dumpStruct({ 'en' => $output}, 0);
_utf8_off($text);
print OUT $text;

close OUT;
